package com.example.drone.api;

import com.example.drone.api.entities.Drone;
import com.example.drone.api.exception.ErrorMessage;
import com.example.drone.api.repos.DroneRepo;
import com.example.drone.api.requestbodies.DroneRequestBody;
import com.example.drone.api.requestbodies.MedicationRequestBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApplicationTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DroneRepo droneRepo;
    private final String DRONE_SERIAL_NUMBER = "1234567890";

    @Test
    @Order(0)
    void contextLoads() {
    }

    @Test
    @Order(1)
    void registerDrone() throws Exception {
        mockMvc.perform(post("/drones")
                        .content(new ObjectMapper().writeValueAsString(DroneRequestBody.builder()
                                .serialNumber(DRONE_SERIAL_NUMBER)
                                .model(Drone.Model.LIGHTWEIGHT)
                                .weightLimit(new BigDecimal("450.00"))
                                .batteryCapacity(100)
                                .state(Drone.State.IDLE)
                                .build()))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    @Test
    @Order(1)
    void checkDroneBatteryCapacity() throws Exception {
        Drone drone = droneRepo.findBySerialNumber(DRONE_SERIAL_NUMBER).orElseThrow();
        mockMvc.perform(get("/drones/" + drone.getId() + "/batteryCapacity")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(100));
    }
    @Test
    @Order(2)
    void checkDronesAvailableForLoadingBeforeLoading() throws Exception {
        mockMvc.perform(get("/drones-available-for-loading")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].serialNumber").value(DRONE_SERIAL_NUMBER));
    }

    @Test
    @Order(3)
    void loadMedication() throws Exception {
        Drone drone = droneRepo.findBySerialNumber(DRONE_SERIAL_NUMBER).orElseThrow();
        mockMvc.perform(post("/drones/" + drone.getId() + "/medications")
                        .content(new ObjectMapper().writeValueAsString(MedicationRequestBody.builder()
                                .name("PARACETAMOL_TABS")
                                .weight(drone.getWeightLimit().divide(BigDecimal.TEN, 2, RoundingMode.CEILING))
                                .code("PARAT")
                                .image("https://www.pexels.com/photo/pill-and-caplet-161449/")
                                .build()))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    @Test
    @Order(4)
    void loadMedicationAfterWeightLimitIsReached() throws Exception {
        Drone drone = droneRepo.findBySerialNumber(DRONE_SERIAL_NUMBER).orElseThrow();
        mockMvc.perform(post("/drones/" + drone.getId() + "/medications")
                        .content(new ObjectMapper().writeValueAsString(MedicationRequestBody.builder()
                                .name("METRONIDAZOLE_TABS")
                                .weight(drone.getWeightLimit())
                                .code("METROT")
                                .image("https://www.pexels.com/photo/pill-and-caplet-161449/")
                                .build()))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(ErrorMessage.WEIGHT_LIMIT_EXCEEDED.getMessage()));
    }
    @Test
    @Order(4)
    void checkDronesAvailableForLoadingAfterLoading() throws Exception {
        mockMvc.perform(get("/drones-available-for-loading")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(1));
    }
    @Test
    @Order(5)
    void checkLoadedMedications() throws Exception {
        Drone drone = droneRepo.findBySerialNumber(DRONE_SERIAL_NUMBER).orElseThrow();
        mockMvc.perform(get("/drones/" + drone.getId() + "/medications")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].name").value("PARACETAMOL_TABS"));
    }
    @Test
    @Order(6)
    void loadMedicationWhenBatteryIsBelow25Percent() throws Exception {
        Drone drone = droneRepo.findBySerialNumber(DRONE_SERIAL_NUMBER).orElseThrow();
        drone.setBatteryCapacity(21);
        droneRepo.save(drone);
        mockMvc.perform(post("/drones/" + drone.getId() + "/medications")
                        .content(new ObjectMapper().writeValueAsString(MedicationRequestBody.builder()
                                .name("METRONIDAZOLE_TABS")
                                .weight(drone.getWeightLimit().divide(BigDecimal.TEN, 2, RoundingMode.CEILING))
                                .code("METROT")
                                .image("https://www.pexels.com/photo/pill-and-caplet-161449/")
                                .build()))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value(ErrorMessage.BATTERY_CAPACITY_BELOW_25.getMessage()));
    }
}
