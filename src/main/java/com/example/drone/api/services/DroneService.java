package com.example.drone.api.services;

import com.example.drone.api.entities.Drone;
import com.example.drone.api.entities.Medication;
import com.example.drone.api.exception.AppException;
import com.example.drone.api.exception.ErrorMessage;
import com.example.drone.api.repos.DroneRepo;
import com.example.drone.api.repos.MedicationRepo;
import com.example.drone.api.requestbodies.DroneRequestBody;
import com.example.drone.api.requestbodies.MedicationRequestBody;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class DroneService {
    private final DroneRepo droneRepo;
    private final MedicationRepo medicationRepo;

    public Drone register(DroneRequestBody requestBody) {
        return droneRepo.save(Drone.builder()
                .serialNumber(requestBody.getSerialNumber())
                .model(requestBody.getModel())
                .weightLimit(requestBody.getWeightLimit())
                .batteryCapacity(requestBody.getBatteryCapacity())
                .state(requestBody.getState())
                .build());
    }
    @Transactional
    public Medication loadMedication(Integer droneId, MedicationRequestBody requestBody) {
        Drone drone = droneRepo.findById(droneId).orElseThrow();
        BigDecimal totalMedicationWeight = drone.getMedications().stream()
                .map(Medication::getWeight)
                .reduce(requestBody.getWeight(), BigDecimal::add);
        if (totalMedicationWeight.compareTo(drone.getWeightLimit()) > 0)
            throw new AppException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMessage.WEIGHT_LIMIT_EXCEEDED.getMessage());
        if (drone.getBatteryCapacity() < 25) {
            drone.setState(Drone.State.IDLE);
            droneRepo.save(drone);
            throw new AppException(HttpStatus.UNPROCESSABLE_ENTITY, ErrorMessage.BATTERY_CAPACITY_BELOW_25.getMessage());
        }
        drone.setState(totalMedicationWeight.equals(drone.getWeightLimit()) ? Drone.State.LOADED : Drone.State.LOADING);
        droneRepo.save(drone);
        return medicationRepo.save(Medication.builder()
                .name(requestBody.getName())
                .weight(requestBody.getWeight())
                .code(requestBody.getCode())
                .image(requestBody.getImage())
                .drone(drone)
                .build());}
}
