package com.example.drone.api.controllers;

import com.example.drone.api.entities.BatteryCapacityLog;
import com.example.drone.api.entities.Drone;
import com.example.drone.api.entities.Medication;
import com.example.drone.api.repos.DroneRepo;
import com.example.drone.api.requestbodies.DroneRequestBody;
import com.example.drone.api.requestbodies.MedicationRequestBody;
import com.example.drone.api.services.DroneService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class Controller {
    private final DroneService droneService;
    private final DroneRepo droneRepo;
    @PostMapping("/drones")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Drone registerDrone(@RequestBody @Valid DroneRequestBody requestBody) {
        return droneService.register(requestBody);
    }
    @GetMapping("/drones-available-for-loading")
    public List<Drone> listDronesAvailableForLoading() {
        return droneRepo.findAllByStateIn(List.of(Drone.State.IDLE, Drone.State.LOADING));
    }
    @PostMapping("/drones/{id}/medications")
    public Medication loadDroneWithMedications(@PathVariable Integer id,
                                          @RequestBody @Valid MedicationRequestBody requestBody) {
        return droneService.loadMedication(id, requestBody);
    }
    @GetMapping("/drones/{id}/medications")
    public List<Medication> getLoadedMedications(@PathVariable Integer id) {
        return droneRepo.findById(id).orElseThrow().getMedications();
    }
    @GetMapping("/drones/{id}/batteryCapacityLogs")
    public List<BatteryCapacityLog> getBatteryCapacityLogs(@PathVariable Integer id) {
        return droneRepo.findById(id).orElseThrow().getBatteryCapacityLogs();
    }
    @GetMapping("/drones/{id}/batteryCapacity")
    public Integer getDroneBatteryCapacity(@PathVariable Integer id) {
        return droneRepo.findById(id).orElseThrow().getBatteryCapacity();
    }
}
