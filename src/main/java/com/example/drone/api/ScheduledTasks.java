package com.example.drone.api;

import com.example.drone.api.entities.BatteryCapacityLog;
import com.example.drone.api.repos.BatteryCapacityLogRepo;
import com.example.drone.api.repos.DroneRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ScheduledTasks {
    private final BatteryCapacityLogRepo batteryCapacityLogRepo;
    private final DroneRepo droneRepo;
    @Scheduled(fixedRate = 50000)
    @EventListener(ApplicationReadyEvent.class)
    public void createBatteryCapacityLogs() {
        batteryCapacityLogRepo.saveAll(droneRepo.findAll().stream()
                .map(drone -> BatteryCapacityLog.builder()
                        .drone(drone)
                        .batteryCapacity(drone.getBatteryCapacity())
                        .timestamp(Timestamp.from(Instant.now()))
                        .build())
                .collect(Collectors.toList()));
    }
}
