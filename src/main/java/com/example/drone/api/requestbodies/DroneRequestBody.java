package com.example.drone.api.requestbodies;

import com.example.drone.api.entities.Drone;
import jakarta.validation.constraints.*;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DroneRequestBody {
    @NotEmpty(message = "Serial number is required")
    @Size(max = 100)
    private String serialNumber;
    @NotNull(message = "Model is required")
    private Drone.Model model;
    @NotNull(message = "Weight limit is required")
    @DecimalMin(value = "0", inclusive = false, message = "Weight limit must be between 0 and 500")
    @DecimalMax(value = "500", message = "Weight limit must be between 0 and 500")
    @Digits(integer = 3, fraction = 2, message = "Weight limit must be between 0 and 500")
    private BigDecimal weightLimit;
    @NotNull(message = "Battery capacity is required")
    @Min(value = 0, message = "Batter capacity must be between 0 and 100")
    @Max(value = 100, message = "Batter capacity must be between 0 and 100")
    private Integer batteryCapacity;
    @NotNull(message = "State is required")
    private Drone.State state;
}
