package com.example.drone.api.requestbodies;

import jakarta.validation.constraints.*;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MedicationRequestBody {
    @NotEmpty(message = "Name is required")
    @Pattern(regexp = "^[\\w-]*$", message = "Name can only contain letters, numbers, hyphens or underscores")
    private String name;
    @NotNull(message = "Weight is required")
    @DecimalMin(value = "0", inclusive = false, message = "Weight must be between 0 and 500")
    @DecimalMax(value = "500", message = "Weight must be between 0 and 500")
    @Digits(integer = 3, fraction = 2, message = "Weight must be between 0 and 500")
    private BigDecimal weight;
    @NotEmpty(message = "Code is required")
    @Pattern(regexp = "^[A-Z0-9_]*$", message = "Code can only contain upper case letters, numbers or underscores")
    private String code;
    private String image;
}
