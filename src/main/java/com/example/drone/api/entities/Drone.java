package com.example.drone.api.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Digits;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true, length = 100, nullable = false)
    private String serialNumber;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Model model;
    @Digits(integer = 3, fraction = 2)
    @Column(nullable = false)
    private BigDecimal weightLimit;
    @Column(nullable = false)
    private Integer batteryCapacity;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private State state = State.IDLE;
    @OneToMany(mappedBy = "drone")
    private List<Medication> medications;
    @OneToMany(mappedBy = "drone")
    private List<BatteryCapacityLog> batteryCapacityLogs;

    public enum Model {
        LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT;
    }
    public enum State {
        IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
    }
}
