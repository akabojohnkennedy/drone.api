package com.example.drone.api.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ErrorResponseBody {
    private Timestamp timestamp;
    private String message;
    private List<String> errors;

    public ErrorResponseBody(String message, List<String> errors) {
        this.timestamp = Timestamp.from(Instant.now());
        this.message = message;
        this.errors = errors;
    }

    public ErrorResponseBody(String message) {
        this(message, new ArrayList<>());
    }
}
