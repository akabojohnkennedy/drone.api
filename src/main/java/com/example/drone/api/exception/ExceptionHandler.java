package com.example.drone.api.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ErrorResponseBody handle(MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        String message = result.getFieldError().getDefaultMessage();
        List<String> errors = new ArrayList<>();
        for (FieldError error : result.getFieldErrors()) {
            errors.add(error.getDefaultMessage());
        }
        return new ErrorResponseBody(Timestamp.from(Instant.now()), message, errors);
    }
    @org.springframework.web.bind.annotation.ExceptionHandler(AppException.class)
    public final ResponseEntity<ErrorResponseBody> handle(AppException e) {
        return ResponseEntity.status(e.getStatus())
                .body(new ErrorResponseBody(Timestamp.from(Instant.now()), e.getMessage(), new ArrayList<>()));
    }
}