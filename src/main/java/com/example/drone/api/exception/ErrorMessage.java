package com.example.drone.api.exception;

public enum ErrorMessage {
    WEIGHT_LIMIT_EXCEEDED("Weight limit exceeded"),
    BATTERY_CAPACITY_BELOW_25("Battery capacity is below 25%");
    private final String message;

    ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
