package com.example.drone.api.repos;

import com.example.drone.api.entities.Drone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DroneRepo extends JpaRepository<Drone, Integer> {
    Optional<Drone> findBySerialNumber(String serialNumber);
    List<Drone> findAllByStateIn(List<Drone.State> states);
}
