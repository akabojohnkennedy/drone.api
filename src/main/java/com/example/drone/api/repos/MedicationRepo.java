package com.example.drone.api.repos;

import com.example.drone.api.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepo extends JpaRepository<Medication, Integer> {
}
