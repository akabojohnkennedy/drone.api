package com.example.drone.api.repos;

import com.example.drone.api.entities.BatteryCapacityLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BatteryCapacityLogRepo extends JpaRepository<BatteryCapacityLog, UUID> {
}
