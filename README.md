### REQUIREMENTS
- JDK 17
- MAVEN

### BUILD & RUN INSTRUCTIONS
1. `mvn test` to run tests
2. `mvn spring-boot:run` to run application on port `8080`

### ENDPOINTS
- Register drone
```
POST http://localhost:8080/drones
Content-Type: application/json
```
```json
{
  "serialNumber": "1234567890",
  "model": "LIGHTWEIGHT",
  "weightLimit": 250,
  "batteryCapacity": 100,
  "state": "IDLE"
}
```
- Load drone with medication
```
POST http://localhost:8080/drones/{id}/medications
Content-Type: application/json
```
```json
{
  "name": "PARACETAMOL_TABS",
  "weight": 250,
  "code": "PRT",
  "image": "https://www.pexels.com/photo/pill-and-caplet-161449/"
}
```
- Check loaded medication items for a given drone
```
GET http://localhost:8080/drones/{id}/medications
```
- Check drones available for loading
```
GET http://localhost:8080/drones-available-for-loading
```
- Check drone battery level
```
GET http://localhost:8080/drones/{id}/batteryCapacity
```
- Check drone battery level logs
```
GET http://localhost:8080/drones/{id}/batteryCapacityLogs
```
